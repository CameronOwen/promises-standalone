﻿using System;
using JetBrains.Annotations;

namespace Promises
{

    public interface IPromise<TPromised> : IPendingPromise<TPromised>
    {

        PromiseState PromiseState { get; }


        IPromise<TPromised> Catch([NotNull] Action<Exception> onRejected);


        void Done();


        void Done([NotNull] Action<TPromised> action);


        /// <summary>
        ///     Assigns a name to this promise and any chained promises.
        ///     Useful for debugging.
        /// </summary>
        /// <returns>The promise instance for method chaining</returns>
        IPromise<TPromised> WithName(string name);


        /// <summary>
        ///     Chains an action passing in the result of the previous promise
        /// </summary>
        /// <returns>A new promise wraping the action.</returns>
        IPromise Then([NotNull] Action action);

        /// <summary>
        ///     Chains an action passing in the result of the previous promise
        /// </summary>
        /// <returns>A new promise wraping the action.</returns>
        IPromise<TPromised> Then([NotNull] Action<TPromised> action);


        /// <summary>
        ///     Chains a non-value promise, discarding the result of the
        ///     previous promise.
        /// </summary>
        /// <returns>A new promise wrapping the func.</returns>
        IPromise<TPromised> Then([NotNull] Func<IPromise> nonValuePromise);


        /// <summary>
        ///     Chains a promise discarding the result of the previous promise.
        /// </summary>
        /// <returns></returns>
        IPromise<TPromised> Then([NotNull] Func<IPromise<TPromised>> promise);


        /// <summary>
        ///     Chains a promise passing in the result of the previous promise.
        /// </summary>
        IPromise<TPromised> Then([NotNull] Func<TPromised, IPromise<TPromised>> promise);


        /// <summary>
        ///     Chains a non-value promise, passing in the result of the
        ///     previous promise.
        /// </summary>
        IPromise Then([NotNull] Func<TPromised, IPromise> nonValuePromise);


        IPromise<TConverted> Then<TConverted>([NotNull] Func<TPromised, TConverted> action);


        IPromise<TConverted> Then<TConverted>([NotNull] Func<TPromised, IPromise<TConverted>> promise);

    }

}