﻿using System;

namespace Promises
{

    public interface IRejectable
    {

        /// <summary>
        /// </summary>
        /// <param name="ex"></param>
        void Reject(Exception ex);

    }

}