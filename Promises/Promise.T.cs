﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Promises
{

    public sealed class Promise<TPromised> : AbstractPromise, IPromise<TPromised>
    {

        private static readonly object staticLock = new object();
        private static readonly Queue<Promise<TPromised>> pool;
        private static uint promiseCounter;

        private readonly List<Action<TPromised>> resolveHandlers;
        private TPromised resolveValue;

        [UsedImplicitly]
        private uint id;

        /// <summary>
        ///     Static initializer
        /// </summary>
        static Promise()
        {
            pool = new Queue<Promise<TPromised>>(32);
            for (var i = 0; i < 32; i++)
            {
                pool.Enqueue(new Promise<TPromised>());
            }
        }


        /// <summary>
        ///     Internal Promise Constructor
        /// </summary>
        private Promise()
        {
            id = promiseCounter++;
            resolveHandlers = new List<Action<TPromised>>();
        }


        public IPromise<TPromised> WithName(string name)
        {
            Name = name;
            return this;
        }


        public override void Reject(Exception ex)
        {
            if (PromiseState != PromiseState.Pending)
            {
                throw new InvalidPromiseStateException("Unable to reject " + this + " as it is already in state " + PromiseState);
            }
            PromiseState = PromiseState.Rejected;
            exception = ex;
            if (TryReject())
            {
                exception = null;
                exceptionHandler = null;
            }
            else if (isDone)
            {
                throw ex;
            }
        }


        public void Resolve(TPromised value)
        {
            if (PromiseState != PromiseState.Pending)
            {
                throw new InvalidPromiseStateException("Unable to resolve " + this + " as it is already in state " + PromiseState);
            }
            resolveValue = value;
            PromiseState = PromiseState.Resolved;
            if (!isDone)
            {
                return;
            }
            if (TryResolve())
            {
                Recycle();
            }
        }


        public IPromise<TPromised> Catch(Action<Exception> onRejected)
        {
            exceptionHandler = onRejected;

            if (PromiseState != PromiseState.Rejected)
            {
                return this;
            }
            if (!TryReject())
            {
                return this;
            }
            Recycle();
            var p = Create();
            p.PromiseState = PromiseState.Rejected;
            return p;
        }


        public void Done()
        {
            isDone = true;
            switch (PromiseState)
            {
                case PromiseState.Pending:
                    return;
                case PromiseState.Resolved:
                    TryResolve();
                    Recycle();
                    break;
                case PromiseState.Rejected:
                    if (!TryReject() && exception != null)
                    {
                        throw exception;
                    }
                    Recycle();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Done(Action action)
        {
            resolveHandlers.Add(v => { action(); });
            Done();
        }


        public void Done(Action<TPromised> action)
        {
            resolveHandlers.Add(action);
            Done();
        }


        public IPromise Then(Action action)
        {
            var p = Promise.Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    action();
                    p.Resolve();
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }

        public IPromise<TPromised> Then(Action<TPromised> action)
        {
            var p = Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    action(v);
                    p.Resolve(v);
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }


        public IPromise Then(Func<TPromised, IPromise> nonValuePromise)
        {
            var p = Promise.Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    nonValuePromise(v).Done(p.Resolve);
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }


        public IPromise<TPromised> Then(Func<IPromise> nonValuePromise)
        {
            var p = Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    nonValuePromise().Done(() => { p.Resolve(v); });
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }


        public IPromise<TPromised> Then(Func<IPromise<TPromised>> promise)
        {
            var p = Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    promise().Done(p.Resolve);
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }


        public IPromise<TPromised> Then(Func<TPromised, IPromise<TPromised>> promise)
        {
            var p = Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    promise(v).Done(p.Resolve);
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }


        public IPromise<TConverted> Then<TConverted>(Func<TPromised, TConverted> action)
        {
           var p = Promise<TConverted>.Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    p.Resolve(action(v));
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }


        public IPromise<TConverted> Then<TConverted>(Func<TPromised, IPromise<TConverted>> promise)
        {
            var p = Promise<TConverted>.Create(exceptionHandler);
            Done(v =>
            {
                try
                {
                    promise(v).Done(p.Resolve);
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }
            });
            return p;
        }




        protected internal override void Recycle()
        {
            if (hasBeenRecycled)
            {
                return;
            }
            hasBeenRecycled = true;
            Name = string.Empty;
            resolveValue = default(TPromised);
            isDone = false;
            resolveHandlers.Clear();
            exceptionHandler = null;
            exception = null;
            lock (staticLock)
            {
                pool.Enqueue(this);
            }
        }

        private bool TryResolve()
        {
            try
            {
                foreach (var resolveHandler in resolveHandlers)
                {
                    resolveHandler(resolveValue);
                }
                resolveHandlers.Clear();
                return true;
            }
            catch (Exception ex)
            {
                if (!TryReject(ex))
                {
                    throw;
                }
            }
            return false;
        }


        public static Promise<TPromised> Create()
        {
            lock (staticLock)
            {
                var promise = pool.Count > 0 ? pool.Dequeue() : new Promise<TPromised>();
                promise.PromiseState = PromiseState.Pending;
                promise.hasBeenRecycled = false;
                return promise;
            }
        }


        internal static Promise<TPromised> Create(Action<Exception> exceptionHandler)
        {
            var promise = Create();
            promise.exceptionHandler = exceptionHandler;
            return promise;
        }


        public static Promise<TPromised> Resolved(TPromised value)
        {
            var promise = Create();
            promise.Resolve(value);
            return promise;
        }


        public static Promise<TPromised> Rejected(Exception ex)
        {
            var promise = Create();
            promise.Reject(ex);
            return promise;
        }



    }

}