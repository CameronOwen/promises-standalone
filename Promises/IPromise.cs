﻿using System;
using JetBrains.Annotations;

namespace Promises
{

    public interface IPromise : IPendingPromise
    {

        PromiseState PromiseState { get; }


        IPromise Catch([NotNull] Action<Exception> onRejected);


        void Done();


        void Done([NotNull] Action action);


        IPromise WithName(string name);


        IPromise Then([NotNull] Action action);


        IPromise Then([NotNull] Func<IPromise> promise);


        //IPromise<TConverted> Then<TConverted>([NotNull] Func<IPromise<TConverted>> next);
        IPromise<TConverted> Then<TConverted>([NotNull] Func<IPromise<TConverted>> promise);

    }

}