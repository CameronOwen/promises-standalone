﻿namespace Promises
{

    public enum PromiseState
    {

        Pending = 0,
        Resolved,
        Rejected,
    }

}