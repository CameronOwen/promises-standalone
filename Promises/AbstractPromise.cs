using System;
using System.Collections.Generic;

namespace Promises
{

    public abstract class AbstractPromise : IRejectable
    {

        protected bool isDone;
        protected bool hasBeenRecycled = false;

        protected Action<Exception> exceptionHandler;
        protected Exception exception;

        public string Name { get; protected set; }

        public PromiseState PromiseState { get; protected set; }


        public abstract void Reject(Exception ex);


        protected internal abstract void Recycle();


        protected bool TryReject(Exception exception)
        {
            if (this.exception != null)
            {
                throw new InvalidPromiseStateException("Multiple exceptions encountered");
            }

            this.exception = exception;
            return TryReject();
        }

        protected bool TryReject()
        {
            if (exceptionHandler == null || exception == null)
            {
                return false;
            }
            exceptionHandler(exception);
            return true;
        }

    }

}
 