﻿using System;

namespace Promises
{

    public class PromiseException : Exception
    {

        public PromiseException(string message) : base(message) {}

    }

}