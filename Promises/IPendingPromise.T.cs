﻿using JetBrains.Annotations;

namespace Promises
{

    public interface IPendingPromise<in TPromised> : IRejectable
    {

        void Resolve([NotNull] TPromised value);

    }

}