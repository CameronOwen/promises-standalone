﻿namespace Promises
{

    public class InvalidPromiseStateException : PromiseException
    {

        public InvalidPromiseStateException(string message) : base(message) {}

    }

}