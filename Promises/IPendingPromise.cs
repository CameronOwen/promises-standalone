﻿namespace Promises
{

    public interface IPendingPromise : IRejectable
    {

        void Resolve();

    }

}