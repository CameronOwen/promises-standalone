﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Promises
{

    public sealed class Promise : AbstractPromise, IPromise
    {

        private static readonly object staticLock = new object();
        private static readonly Queue<Promise> pool;
        private static uint promiseCounter;

        private readonly List<Action> resolveHandlers;

        public static bool EventsPropergateForwards { get; set; }

        [UsedImplicitly]
        private uint id;

        static Promise()
        {
            pool = new Queue<Promise>(128);
            for (var i = 0; i < 128; i++)
            {
                pool.Enqueue(new Promise());
            }
        }


        private Promise()
        {
            id = promiseCounter++;
            resolveHandlers = new List<Action>();

        }


        public IPromise WithName(string name)
        {
            Name = name;
            return this;
        }


        public override void Reject(Exception ex)
        {
            if (PromiseState != PromiseState.Pending)
            {
                throw new InvalidPromiseStateException("Unable to reject " + this + " as it is already in state " +
                                                       PromiseState);
            }
            PromiseState = PromiseState.Rejected;
            exception = ex;
            if (TryReject())
            {
                exception = null;
                exceptionHandler = null;
            }
            else if (isDone)
            {
                throw ex;
            }

        }




        public void Resolve()
        {
            if (PromiseState != PromiseState.Pending)
            {
                throw new InvalidPromiseStateException("Unable to resolve " + this + " as it is already in state " +
                                                       PromiseState);
            }
            PromiseState = PromiseState.Resolved;
            if (!isDone)
            {
                return;
            }
            if (TryResolve())
            {
                Recycle();
            }
        }


        public IPromise Catch(Action<Exception> onRejected)
        {
            exceptionHandler = onRejected;

            if (PromiseState != PromiseState.Rejected)
            {
                return this;
            }
            if (!TryReject())
            {
                return this;
            }
            Recycle();
            var p = Create();
            p.PromiseState = PromiseState.Rejected;
            return p;
        }


        public void Done()
        {
            isDone = true;
            switch (PromiseState)
            {
                case PromiseState.Pending:
                    return;
                case PromiseState.Resolved:
                    TryResolve();
                    Recycle();
                    break;
                case PromiseState.Rejected:
                    if (!TryReject() && exception != null)
                    {
                        throw exception;
                    }
                    Recycle();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        public void Done(Action action)
        {
            resolveHandlers.Add(action);
            Done();
        }


        public IPromise Then(Action action)
        {
            var p = Create(exceptionHandler);
            Done(() =>
            {
                action();
                p.Resolve();
            });
            return p;
        }


        public IPromise Then(Func<IPromise> promise)
        {
            var p = Create(exceptionHandler);
            Done(() =>
            {
                    promise().Catch(exceptionHandler).Done(p.Resolve);
            });
            return p;
        }


        public IPromise<TConverted> Then<TConverted>(Func<IPromise<TConverted>> promise)
        {
            var p = Promise<TConverted>.Create(exceptionHandler);
            Done(() =>
            {
                    promise().Catch(exceptionHandler).Done(v => p.Resolve(v));
            });
            return p;
        }


        protected internal override void Recycle()
        {
            hasBeenRecycled = true;
            Name = string.Empty;
            isDone = false;
            resolveHandlers.Clear();
            exceptionHandler = null;
            exception = null;
            lock (staticLock)
            {
                pool.Enqueue(this);
            }
        }

        private bool TryResolve()
        {
            try
            {
                foreach (var resolveHandler in resolveHandlers)
                {
                    resolveHandler();
                }
                resolveHandlers.Clear();
                return true;
            }
            catch (Exception ex)
            {
                if (!TryReject(ex))
                {
                    throw;
                }
            }
            return false;
        }


        public static Promise Create()
        {
            lock (staticLock)
            {
                var promise = pool.Count > 0 ? pool.Dequeue() : new Promise();
                promise.PromiseState = PromiseState.Pending;
                promise.hasBeenRecycled = false;
                return promise;
            }
        }

        internal static Promise Create(Action<Exception> exceptionHandler)
        {
            var promise = Create();
            promise.exceptionHandler = exceptionHandler;
            return promise;
        }


        public static IPromise Resolved()
        {
            var promise = Create();
            promise.Resolve();
            return promise;
        }


        public static IPromise Rejected(Exception ex)
        {
            var promise = Create();
            promise.Reject(ex);
            return promise;
        }

    }

}