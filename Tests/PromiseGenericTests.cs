﻿using System;
using NUnit.Framework;
using Promises;

// ReSharper disable HeapView.BoxingAllocation

namespace Tests
{

    [TestFixture]
    public class PromiseGenericTests
    {

        [Test]
        public void ChainPromiseAndConvertToNonValuePromise()
        {
            var promise = Promise<int>.Create();
            var chainedPromise = Promise.Create();

            var promisedValue = 15;
            var completed = 0;

            promise
                .Then(() => chainedPromise)
                .Then(() => { ++completed; });

            promise.Resolve(promisedValue);
            chainedPromise.Resolve();

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ChainPromiseAndConvertValue()
        {
            var promise = Promise<int>.Create();
            var chainedPromise = Promise<string>.Create();

            var promisedValue = 15;
            var chainedPromiseValue = "blah";
            var completed = 0;

            promise
                .Then(vInt =>
                {
                    Assert.AreEqual(promisedValue, vInt);
                    ++completed;
                }).Then<string>(vInt => chainedPromise)
                .Then(vString =>
                {
                    Assert.AreEqual(chainedPromiseValue, vString);
                    ++completed;
                });

            promise.Resolve(promisedValue);
            chainedPromise.Resolve(chainedPromiseValue);

            Assert.AreEqual(2, completed);
        }


        [Test]
        public void ErrorHandlerIsNotInvokedForResolvedPromise()
        {
            var promise = Promise<bool>.Create();

            promise.Catch(e => { throw new ApplicationException("This shouldn't happen"); });

            promise.Resolve(true);
        }


        [Test]
        public void ExceptionHaltsChainedExecution()
        {
            var ex = new Exception();
            var promise = Promise<bool>.Create();
            var completed = 0;
            promise
                .Then(x =>
                {
                    if (x)
                    {
                        throw ex;
                    }
                })
                .Catch(e1 =>
                {
                    Assert.AreEqual(ex, e1);
                    ++completed;
                })
                .Then(x => { Assert.Fail("This code should not be executed"); })
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++completed;
                })
                .Done();
            promise.Resolve(true);
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ExceptionPropergatesBackwardsToErrorHandler()
        {
            var ex = new Exception();
            var promise = Promise<bool>.Create();
            var completed = 0;
            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++completed;
                })
                .Then(x =>
                {
                    if (x)
                    {
                        throw ex;
                    }
                }).Then(x =>
                {
                    if (!x)
                    {
                        throw ex;
                    }
                })
                .Done();
            promise.Resolve(true);
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ExceptionThrownDuringResolverRejectsPromise()
        {
            var ex = new Exception();
            var promise = Promise<bool>.Create();
            var completed = 0;
            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++completed;
                })
                .Done(v => { throw ex; });
            promise.Resolve(true);
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ExceptionThrownDuringTransformRejectsTransformedPromise()
        {
            var promise = Promise<int>.Create();

            var promisedValue = 15;
            var errors = 0;
            var ex = new Exception();

            promise
                .Then(action: v => { throw ex; })
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                });

            promise.Resolve(promisedValue);

            Assert.AreEqual(1, errors);
        }


        [Test]
        public void ExceptionThrownForRejectAfterReject()
        {
            var promise = Promise<int>.Create();
            promise.Catch(Assert.IsInstanceOf<AssertionException>);
            promise.Reject(new AssertionException("Test"));
            Assert.Throws<InvalidPromiseStateException>(() => promise.Reject(new AssertionException("Test")));
        }


        [Test]
        public void ExceptionThrownForRejectAfterResolve()
        {
            var promise = Promise<int>.Create();
            promise.Resolve(64);
            Assert.Throws<InvalidPromiseStateException>(() => promise.Reject(new AssertionException("Test")));
        }


        [Test]
        public void ExceptionThrownForResolveAfterResolve()
        {
            var promise = Promise<int>.Create();
            promise.Catch(Assert.IsInstanceOf<AssertionException>);
            promise.Reject(new AssertionException("Test"));
            Assert.Throws<InvalidPromiseStateException>(() => promise.Resolve(4));
        }


        // TODO: Fix...
        /*
        [Test]
        public void ChainPromiseFuncs()
        {
            var promise0 = Promise<int>.Create();
            var promise1 = Promise<int>.Create();
            var promise2 = Promise<int>.Create();

            
            var promisedValue = 5;
            Action<int> onComplete = v => { ++v; };
            promise0.Done(onComplete);
            promise1.Done(onComplete);
            promise2.Done(v =>
            {
                Assert.AreEqual(7, v);
            });

            promise0
                .ThenPromise(() =>
                {
                    var p = Promise<int>.Create();
                    p.Done(onComplete);
                    return p;
                })
                .Then(promise2);

            promise0.Resolve(promisedValue);

            Assert.AreEqual(1, completed);
        }
        */


        [Test]
        public void ExceptionThrownInChainRejectsResultingPromise()
        {
            var promise = Promise<int>.Create();
            var chainedPromise = Promise<string>.Create();

            var ex = new Exception();
            var errors = 0;

            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                }).Then<string>(v => chainedPromise)
                .Then(action: v => { throw ex; });

            promise.Resolve(15);
            chainedPromise.Resolve("Blah");
            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectionOfSourcePromiseRejectsChainedPromise()
        {
            var promise = Promise<int>.Create();
            var chainedPromise = Promise<string>.Create();

            var ex = new Exception();
            var errors = 0;

            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                }).Then<string>(v => chainedPromise);

            promise.Reject(ex);

            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectionOfSourcePromiseRejectsTransformedPromise()
        {
            var promise = Promise<int>.Create();

            var ex = new Exception();
            var errors = 0;

            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                })
                .Then(v => v.ToString());

            promise.Reject(ex);

            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectSimple()
        {
            var ex = new Exception();
            var promise = Promise<int>.Rejected(ex);
            var errors = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++errors;
            });
            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectTriggersErrorHandler()
        {
            var promise = Promise<int>.Create();

            var ex = new ApplicationException();
            var completed = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++completed;
            });

            promise.Reject(ex);

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void RejectTriggersPostErrorHandler()
        {
            var promise = Promise<int>.Create();

            var ex = new ApplicationException();
            promise.Reject(ex);

            var completed = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveSimple()
        {
            var completed = 0;
            var promisedValue = 128;
            var promise = Promise<int>.Resolved(128);
            promise.Done(x =>
            {
                Assert.AreEqual(x, promisedValue);
                ++completed;
            });
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveTriggersDone()
        {
            var promise = Promise<int>.Create();

            var completed = 0;
            var promisedValue = 32;

            promise.Done(v =>
            {
                Assert.AreEqual(promisedValue, v);
                ++completed;
            });

            promise.Resolve(promisedValue);
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveTriggersThen()
        {
            var promise = Promise<int>.Create();

            var completed = 0;
            var promisedValue = 32;

            promise.Done(v =>
            {
                Assert.AreEqual(promisedValue, v);
                ++completed;
            });

            promise.Resolve(promisedValue);
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveTriggersThenHandlersInOrder()
        {
            var completed = 0;
            var promise = Promise<int>.Create();

            promise.Then(v => Assert.AreEqual(1, ++completed));
            promise.Then(v => Assert.AreEqual(2, ++completed));
            promise.Resolve(128);

            Assert.AreEqual(2, completed);
        }


        [Test]
        public void ThenHandlerIsNotInvokedForRejectedPromise()
        {
            var promise = Promise<bool>.Create();

            promise.Then(action: () => { throw new ApplicationException("This shouldn't happen"); });

            Assert.Throws<AssertionException>(() => promise.Reject(new AssertionException("Test")));
        }


        [Test]
        public void TransformPromiseValue()
        {
            var promise = Promise<int>.Create();

            var promisedValue = 15;
            var completed = 0;

            promise
                .Then(v => v.ToString())
                .Then(v =>
                {
                    Assert.AreEqual(promisedValue.ToString(), v);
                    ++completed;
                });

            promise.Resolve(promisedValue);

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void UncaughtExceptionsAreThrown()
        {
            var promise = Promise<float>.Create();
            promise.Done();
            Assert.Throws<AssertionException>(() => { promise.Reject(new AssertionException("Test")); });
        }

    }

}