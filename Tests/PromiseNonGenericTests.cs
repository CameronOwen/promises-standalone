﻿using System;
using NUnit.Framework;
using Promises;

namespace Tests
{

    [TestFixture]
    public class PromiseNonGenericTests
    {

        public IPromise FailurePromise()
        {
            var promise = Promise.Create();
            try
            {
                throw new ApplicationException("Test");
            }
            catch (Exception e)
            {
                promise.Reject(e);
            }
            return promise;
        }

    

        public IPromise SimplePromise()
        {
            var promise = Promise.Create();
            var x = 0;
            try
            {
                x++;
                promise.Resolve();
            }
            catch (Exception e)
            {
                promise.Reject(e);
            }
            return promise;
        }

        [Test]
        public void ErrorHandlerInvokedForFailureInChain()
        {

            var errors = 0;
            var done = 0;
            
            SimplePromise()
                .Catch(ex => { ++errors;})
                .Then(promise: SimplePromise)
                .Then(promise: FailurePromise)
                .Done(() =>
                {
                    ++done;
                });


            Assert.AreEqual(1, errors);
            Assert.AreEqual(0, done);
        }


        [Test]
        public void CanChainPromise()
        {
            var promise = Promise.Create();
            var chainedPromise = Promise.Create();

            var completed = 0;

            promise.Then(() => chainedPromise)
                   .Then(() => ++completed)
                   .Done();

            promise.Resolve();
            chainedPromise.Resolve();

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void CaughtExceptionsAreNotThrown()
        {
            var exception = new Exception();

            Func<IPromise> promise = () =>
            {
                var p = Promise.Create();
                try
                {
                    throw exception;
                }
                catch (Exception ex)
                {
                    p.Reject(ex);
                }

                return p;
            };
            var errors = 0;

            Assert.DoesNotThrow(() =>
            {
                promise().Catch(ex =>
                {
                    Assert.AreEqual(ex, exception);
                    ++errors;
                }).Then(() =>
                {
                    ++errors; // Unreachable
                    Assert.Fail();
                }).Done();
            });

            Assert.AreEqual(1, errors);
        }


        [Test]
        public void ChainAndConvertToValuePromise()
        {
            var promise = Promise.Create();
            var chainedPromise = Promise<string>.Create();
            var chainedPromiseValue = "some-value";

            var completed = 0;

            promise
                .Then(() => chainedPromise)
                .Then(v =>
                {
                    Assert.AreEqual(chainedPromiseValue, v);

                    ++completed;
                });

            promise.Resolve();
            chainedPromise.Resolve(chainedPromiseValue);

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void DoneHandlerNotInvokedWithReject()
        {
            var promise = Promise.Create();

            promise.Done(() =>
            {
                throw new ApplicationException("This shouldn't happen");
            });

            Assert.Throws<AssertionException>(() => promise.Reject(new AssertionException("Test")));
        }

        [Test]
        public void DoneHandlerNotInvokedWhenChainRejected()
        {
            var exception = new ApplicationException("Test");
            var promise = Promise.Create();
            var done = false;
            promise.Catch(ex => { Assert.AreEqual(exception, ex); })
                   .Then(() => { throw exception; })
                   .Done(() => { done = true; });

            promise.Resolve();
            Assert.False(done);
        }


        [Test]
        public void ErrorHandlerNotInvokedOnResolve()
        {
            var promise = Promise.Create();
            promise.Catch(ex => { throw ex; });
            promise.Resolve();
        }


        [Test]
        public void ExceptionHaltsChainedExecution()
        {
            var ex = new Exception();
            var promise = Promise.Create();
            var completed = 0;
            var x = 0;
            promise
                .Catch(e1 =>
                {
                    Assert.AreEqual(ex, e1);
                    ++completed;
                }).Then(() =>
                {
                    x++;
                    if (x == 1)
                    {
                        throw ex;
                    }
                })
                
                .Then(() => { Assert.Fail("This code should not be executed"); })
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++completed;
                })
                .Done();
            promise.Resolve();
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ExceptionOnDoneTriggersErrorHandler()
        {
            var ex = new Exception();
            var promise = Promise.Create();
            var errors = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++errors;
            }).Done(() => { throw ex; });

            promise.Resolve();
            Assert.AreEqual(1, errors);
        }


        [Test]
        public void ExceptionPropergatesBackwardsToErrorHandler()
        {
            var ex = new Exception();
            var promise = Promise.Create();
            var completed = 0;
            var x = 0;
            Promise.EventsPropergateForwards = false;
            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++completed;
                })
                .Then(() =>
                {
                    x++;
                    if (x == 0)
                    {
                        throw ex;
                    }
                }).Then(() =>
                {
                    x--;
                    if (x == 0)
                    {
                        throw ex;
                    }
                })
                .Done();
            promise.Resolve();
            Assert.AreEqual(1, completed);
        }



        [Test]
        public void ExceptionsDoNotPropergateForwardsToErrorHandler()
        {
            var exception = new Exception();
            var promise = Promise.Create();
            var completed = 0;
            var x = 0;
            try
            {
                promise
                    .Then(() =>
                    {
                        x++;
                        if (x == 1)
                        {
                            throw exception;
                        }
                    }).Then(() =>
                    {
                        x--;
                        if (x == 2)
                        {
                            throw exception;
                        }
                    })
                    .Catch(e => { Assert.Fail("Control should not flow to here"); })
                    .Done();
                promise.Resolve();
            }
            catch (Exception ex)
            {
                Assert.AreEqual(ex, exception);
            }
        }


        [Test]
        public void ExceptionThrownDuringResolverRejectsPromise()
        {
            var ex = new Exception();
            var promise = Promise.Create();
            var completed = 0;
            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++completed;
                })
                .Done(() => { throw ex; });
            promise.Resolve();
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ExceptionThrownDuringThenRejectsPromise()
        {
            var promise = Promise.Create();

            var errors = 0;
            var ex = new Exception();

            var transformedPromise = promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                })
                .Then(() => { throw ex; });

            promise.Resolve();

            Assert.AreEqual(1, errors);
        }


        [Test]
        public void ExceptionThrownForRejectAfterReject()
        {
            var promise = Promise.Create();
            promise.Catch(Assert.IsInstanceOf<AssertionException>);
            promise.Reject(new AssertionException("Test"));
            Assert.Throws<InvalidPromiseStateException>(() => promise.Reject(new AssertionException("Test")));
        }


        [Test]
        public void ExceptionThrownForRejectAfterResolve()
        {
            var promise = Promise.Create();
            promise.Resolve();
            Assert.Throws<InvalidPromiseStateException>(() => promise.Reject(new AssertionException("Test")));
        }


        [Test]
        public void ExceptionThrownForResolveAfterReject()
        {
            var promise = Promise.Create();
            promise.Reject(new AssertionException("Test"));
            promise.Catch(Assert.IsInstanceOf<AssertionException>);
            Assert.Throws<InvalidPromiseStateException>(() => promise.Resolve());
        }


        [Test]
        public void ExceptionThrownForResolveAfterResolve()
        {
            var promise = Promise.Create();
            promise.Catch(Assert.IsInstanceOf<AssertionException>);
            promise.Reject(new AssertionException("Test"));
            Assert.Throws<InvalidPromiseStateException>(() => promise.Resolve());
        }


        [Test]
        public void ExceptionThrownInChainRejectsResultingPromise()
        {
            var promise = Promise.Create();
            var chainedPromise = Promise.Create();

            var ex = new Exception();
            var errors = 0;

            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                })
                .Then(() => chainedPromise).Then(() => { throw ex; });

            promise.Resolve();
            chainedPromise.Resolve();
            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectionOfSourcePromiseRejectsChainedPromise()
        {
            var promise = Promise.Create().WithName("Source");
            var chainedPromise = Promise.Create().WithName("Chained");

            var ex = new Exception();
            var errors = 0;

            promise
                .Catch(e =>
                {
                    Assert.AreEqual(ex, e);
                    ++errors;
                }).Then(() => chainedPromise);

            promise.Reject(ex);

            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectSimple()
        {
            var ex = new Exception();
            var promise = Promise.Rejected(ex);
            var errors = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++errors;
            });
            Assert.AreEqual(1, errors);
        }


        [Test]
        public void RejectTriggersErrorHandler()
        {
            var promise = Promise.Create();

            var ex = new ApplicationException();
            var completed = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++completed;
            });

            promise.Reject(ex);

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void RejectTriggersPostErrorHandler()
        {
            var promise = Promise.Create();

            var ex = new ApplicationException();
            promise.Reject(ex);

            var completed = 0;
            promise.Catch(e =>
            {
                Assert.AreEqual(ex, e);
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveSimple()
        {
            var completed = 0;
            var promise = Promise.Resolved();
            promise.Done(() => { ++completed; });
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveTriggersDone()
        {
            var promise = Promise.Create();

            var completed = 0;

            promise.Done(() => { ++completed; });

            promise.Resolve();
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveTriggersThen()
        {
            var promise = Promise.Create();

            var completed = 0;

            promise.Then(() => { ++completed; });

            promise.Resolve();
            Assert.AreEqual(1, completed);
        }


        [Test]
        public void ResolveTriggersThenHandlersInOrder()
        {
            var completed = 0;
            var promise = Promise.Create();

            promise.Then(() => Assert.AreEqual(1, ++completed));
            promise.Then(() => Assert.AreEqual(2, ++completed));
            promise.Then(() => Assert.AreEqual(3, ++completed));
            promise.Resolve();

            Assert.AreEqual(3, completed);
        }


        [Test]
        public void ThenHandlerNotInvokedWithReject()
        {
            var promise = Promise.Create();

            promise.Then((Action) (() => { throw new ApplicationException("This shouldn't happen"); }));

            Assert.Throws<AssertionException>(() => promise.Reject(new AssertionException("Test")));
        }


        [Test]
        public void UncaughtExceptionsAreThrown()
        {
            var promise = Promise.Create();
            promise.Done();
            Assert.Throws<AssertionException>(() => { promise.Reject(new AssertionException("Test")); });
        }

    }

}